# IMPALA
Apache Impala is the open source, native analytic database for Apache Hadoop.

>SQL on top HDFS


## Demonstration 





__data__: [covid19-th-confirmed-cases](/data/covid19-th-confirmed-cases.csv)

__impala shell__: 

```sh
impala-shell -k -i hdpndappr1.true.care 
```

```sh
ssh cheewap@hdpmnappr1.true.care

# curl https://data.go.th/dataset/8a956917-436d-4afd-a2d4-59e4dd8e906e/resource/be19a8ad-ab48-4081-b04a-8035b5b2b8d6/download/confirmed-cases.csv --output covid19-th-confirmed-cases.csv

kinit cheewap@TRUE.CARE
hdfs dfs -mkdir -p /tmp/cheewap/data/
hdfs dfs -put covid19-th-confirmed-cases.csv /tmp/cheewap/data/
# hdfs dfs -setfacl -R -m user:impala:rwx /tmp/cheewap/data/
```

Impala create external table

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS rwzcistemp.covid19_confirmed_case_daily_impala
( 
No INT,
announce_date STRING,
Notified_date STRING,
sex STRING,
age INT,
Unit STRING,
nationality STRING,
province_of_isolation STRING,
risk STRING,
province_of_onset STRING,
district_of_onset STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION '/tmp/cheewap/data/'
TBLPROPERTIES ( 'skip.header.line.count'='1', 'skip.footer.line.count'='0' ) ;
```

SQL QUERY
```sql
select * from rwzcistemp.covid19_confirmed_case_daily_impala where limit 10;
select * from rwzcistemp.covid19_confirmed_case_daily_impala where no<=10 limit 10;
```



ref:
https://data.go.th/en/dataset/covid-19-daily