# HBASE (NoSQL)


```sh
sh cheewap@hdpmnappr2.true.care
kinit hdppr1_cisetl1@TRUE.CARE
```


```sh
# create
create_namespace 'hdp_intro'
create 'hdp_intro:table_one', 'col_family'  

list_namespace_tables 'hdp_intro'
list 'hdp_intro:table_one'  

# insert
put 'hdp_intro:table_one', 'row1', 'col_family:a', '1'  
put 'hdp_intro:table_one', 'row1', 'col_family:b', '2'  
put 'hdp_intro:table_one', 'row2', 'col_family:a', '3'  
put 'hdp_intro:table_one', 'row2', 'col_family:b', '4'

# read
scan 'hdp_intro:table_one'  
get 'hdp_intro:table_one', 'row1'  

# update
put 'hdp_intro:table_one', 'row2', 'col_family:b', '4000'
get 'hdp_intro:table_one', 'row2'  
scan 'hdp_intro:table_one'  

# delete
delete 'hdp_intro:table_one', 'row2', 'col_family:b', 1643719903116
delete_all 'hdp_intro:table_one', 'row2', 'col_family:b'

# ex
put 'hdp_intro:table_one', 'row2', 'col_family:b', '5'
put 'hdp_intro:table_one', 'row2', 'col_family:b', '6'
get 'hdp_intro:table_one', 'row2', {COLUMN => 'col_family:b'}

scan 'hdp_intro:table_one', {COLUMNS => 'col_family:b', TIMERANGE => [1643719903116, 1643720577577]}
 
```


Hive

```sql
CREATE EXTERNAL TABLE prod1_bigdatacdr_opr.tbl_cdr_partition_audit (
  key STRING COMMENT 'from deserializer',
  info_cdr_filename STRING COMMENT 'from deserializer',
  info_counter BIGINT COMMENT 'from deserializer',
  info_feed_name STRING COMMENT 'from deserializer',
  info_modified_datetime TIMESTAMP COMMENT 'from deserializer',
  info_modified_wf_id STRING COMMENT 'from deserializer',
  info_partition STRING COMMENT 'from deserializer',
  info_registered_datetime TIMESTAMP COMMENT 'from deserializer',
  info_registered_wf_id STRING COMMENT 'from deserializer',
  info_table STRING COMMENT 'from deserializer'
)
STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'
WITH SERDEPROPERTIES (
    'hbase.columns.mapping'=':key
    ,\r\ninfo:cdr_filename#s
    ,\r\ninfo:feed_name#s
    ,\r\ninfo:counter#b
    ,\r\ninfo:modified_datetime#s
    ,\r\ninfo:modified_wf_id#s
    ,\r\ninfo:partition#s
    ,\r\ninfo:registered_datetime#s
    ,\r\ninfo:registered_wf_id#s
    ,\r\ninfo:table#s', 'serialization.format'='1'
)
TBLPROPERTIES ('COLUMN_STATS_ACCURATE'='false', 'hbase.table.name'='prod1_bigdatacdr_opr:cdr_partition_audit', 'numFiles'='0', 'numRows'='-1', 'rawDataSize'='-1', 'storage_handler'='org.apache.hadoop.hive.hbase.HBaseStorageHandler', 'totalSize'='0')	

```