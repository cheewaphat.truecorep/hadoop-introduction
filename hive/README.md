# Hive
Apache Hive is a data warehouse software project built on top of Apache Hadoop for providing data query and analysis. Hive gives an SQL-like interface to query data stored in various databases and file systems that integrate with Hadoop

>SQL on top HDFS



## Demonstration 



__Monitor)__: [ResourceManager](http://hdpfsappr1.true.care:8088/cluster)<br/>

__data__: [covid19-th-confirmed-cases](/data/covid19-th-confirmed-cases.csv)

__beeline__: 

```sh
beeline -u "jdbc:hive2://hdppr1-hive.true.care:10000/default;principal=hive/hdppr1-hive.true.care@TRUE.CARE"
```

```sh
ssh cheewap@hdpmnappr1.true.care

# curl https://data.go.th/dataset/8a956917-436d-4afd-a2d4-59e4dd8e906e/resource/be19a8ad-ab48-4081-b04a-8035b5b2b8d6/download/confirmed-cases.csv --output covid19-th-confirmed-cases.csv

kinit cheewap@TRUE.CARE
hdfs dfs -mkdir -p /tmp/cheewap/data/
hdfs dfs -put covid19-th-confirmed-cases.csv /tmp/cheewap/data/
hdfs dfs -setfacl -R -m user:hive:rwx /tmp/cheewap/data/
```

hive create external table
```sql
CREATE EXTERNAL TABLE IF NOT EXISTS rwzcistemp.covid19_confirmed_case_daily_serde
( 
No INT,
announce_date STRING,
Notified_date STRING,
sex STRING,
age INT,
Unit STRING,
nationality STRING,
province_of_isolation STRING,
risk STRING,
province_of_onset STRING,
district_of_onset STRING
)
PARTITIONED BY (LOADDATE INT, PID INT)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
WITH SERDEPROPERTIES ( 'line.delim'='\n' , 'field.delim'=',' , 'serialization.format'=',' ,'quoteChar' = '"' ,'escapeChar' = '\\' )
STORED AS TEXTFILE
LOCATION '/tmp/cheewap/data/'
TBLPROPERTIES ( 'skip.header.line.count'='1', 'skip.footer.line.count'='0' ) ;
```

```sql
CREATE EXTERNAL TABLE IF NOT EXISTS rwzcistemp.covid19_confirmed_case_daily
( 
No INT,
announce_date STRING,
Notified_date STRING,
sex STRING,
age INT,
Unit STRING,
nationality STRING,
province_of_isolation STRING,
risk STRING,
province_of_onset STRING,
district_of_onset STRING
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'
STORED AS TEXTFILE
LOCATION '/tmp/cheewap/data/'
TBLPROPERTIES ( 'skip.header.line.count'='1', 'skip.footer.line.count'='0' ) ;

```

SQL QUERY
```sql
select * from rwzcistemp.covid19_confirmed_case_daily where no=1 limit 10;
```



ref:
https://data.go.th/en/dataset/covid-19-daily