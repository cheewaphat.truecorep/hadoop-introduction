# HDFS

## command
HDFS is the primary or major component of the Hadoop ecosystem which is responsible for storing large data sets of structured or unstructured data across various nodes and thereby maintaining the metadata in the form of log files. To use the HDFS commands, first you need to start the Hadoop services using the following command:

__Commands:__

1. __ls__: This command is used to list all the files. Use lsr for recursive approach. It is useful when we want a hierarchy of a folder.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -ls  <path>
    ```
    Example:
    ```
    bin/hdfs dfs -ls /  
    ```
    It will print all the directories present in HDFS. bin directory contains executables so, bin/hdfs means we want the executables of hdfs particularly dfs(Distributed File System) commands.



2. __mkdir__: To create a directory. In Hadoop dfs there is no home directory by default. So let’s first create it.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -mkdir <folder name>
    ```
    creating home directory:
    ```sh
    hdfs/bin -mkdir /user
    hdfs/bin -mkdir /user/username -> write the username of your computer 
    ```
    Example:
    ```sh
    bin/hdfs dfs -mkdir  /geeks  =>  '/' means absolute path
    bin/hdfs dfs -mkdir  geeks2  =>   Relative path -> the folder will be 
                                    created relative to the home directory.
    ```



3. __touchz__: It creates an empty file.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs  -touchz  <file_path>
    ```
    Example:
    ```sh
    bin/hdfs dfs -touchz  /geeks/myfile.txt 
    ```

4. __copyFromLocal (or) put__: To copy files/folders from local file system to hdfs store. This is the most important command. Local filesystem means the files present on the OS.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -copyFromLocal <local file path>  <dest(present on hdfs)>
    ```
    Example: Let’s suppose we have a file AI.txt on Desktop which we want to copy to folder geeks present on hdfs.
    ```sh
    bin/hdfs dfs -copyFromLocal ../Desktop/AI.txt /geeks
    ```
    (OR)
    ```sh
    bin/hdfs dfs -put ../Desktop/AI.txt /geeks
    ```

5. __cat__: To print file contents.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -cat <path>
    ```
    Example:
    ```sh
    // print the content of AI.txt present
    // inside geeks folder.
    bin/hdfs dfs -cat /geeks/AI.txt ->
    ```

6. __copyToLocal (or) get__: To copy files/folders from hdfs store to local file system.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -copyToLocal  <<srcfile(on hdfs)> <local file dest>
    ```
    Example:
    ```sh
    bin/hdfs dfs -copyToLocal  /geeks   ../Desktop/hero  
    ```
    (OR)
    ```sh
    bin/hdfs dfs -get /geeks/myfile.txt  ../Desktop/hero
    ```
    myfile.txt from geeks folder will be copied to folder hero present on Desktop.


    >Note: Observe that we don’t write bin/hdfs while checking the things present on local filesystem.

7. __moveFromLocal__: This command will move file from local to hdfs.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -moveFromLocal <local src>   <dest(on hdfs)> 
    ```
    Example:
    ```sh
    bin/hdfs dfs -moveFromLocal  ../Desktop/cutAndPaste.txt   /geeks
    ```

8. __cp__: This command is used to copy files within hdfs. Lets copy folder geeks to geeks_copied.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -cp  <src(on hdfs)>  <dest(on hdfs)>
    ```
    Example:
    ```sh
    bin/hdfs -cp /geeks  /geeks_copied
    ```

9. __mv__: This command is used to move files within hdfs. Lets cut-paste a file myfile.txt from geeks folder to geeks_copied.
<br/>Syntax:
    ```sh
    bin/hdfs dfs -mv  <src(on hdfs)> <src(on hdfs)>
    ```
    Example:
    ```sh
    bin/hdfs  -mv  /geeks/myfile.txt  /geeks_copied
    ```

10. __rmr__: This command deletes a file from HDFS recursively. It is very useful command when you want to delete a non-empty directory.
    <br/>Syntax:
    ```sh
    bin/hdfs dfs -rmr <filename/directoryName>
    ```
    Example:
    ```sh
    bin/hdfs dfs -rmr  /geeks_copied -> It will delete all the content inside the 
                                        directory then the directory itself.
    ```

11. __du__: It will give the size of each file in directory.
<br/>Syntax:
    ```sh
    bin/hdfs dfs -du  <dirName>
    ```
    Example:
    ```sh
    bin/hdfs dfs -du /geeks
    ```

12. __dus__:: This command will give the total size of directory/file.
<br/>Syntax:
    ```sh
    bin/hdfs dfs -dus  <dirName>
    ```
    Example:
    ```sh
    bin/hdfs dfs -dus /geeks
    ```

13. __stat__: It will give the last modified time of directory or path. In short it will give stats of the directory or file.
<br/>Syntax:
    ```sh
    bin/hdfs  dfs -stat    <hdfs file>
    ```
    Example:
    ```sh
    bin/hdfs dfs -stat /geeks
    ```

14. __setrep__: This command is used to change the replication factor of a file/directory in HDFS. By default it is 3 for anything which is stored in HDFS (as set in hdfs core-site.xml).
    Example 1: To change the replication factor to 6 for geeks.txt stored in HDFS.
    ```sh
    bin/hdfs dfs -setrep -R -w 6 geeks.txt
    ```
    Example 2: To change the replication factor to 4 for a directory geeksInput stored in HDFS.
    ```sh
    bin/hdfs dfs -setrep -R  4 /geeks
    ```
    >Note: The -w means wait till the replication is completed. And -R means recursively, we use it for directories as they may also contain many files and folders inside them.


    >Note: There are more commands in HDFS but we discussed the commands which are commonly used when working with Hadoop. You can check out the list of dfs commands using the following command:


# Demonstration

```sh
kinit cheewap@TRUE.CARE
hdfs dfs -mkdir -p /user/cheewap/hadoop-introduction
hdfs dfs -touchz /user/cheewap/hadoop-introduction/hello.txt
hdfs dfs -ls /user/cheewap/hadoop-introduction/
echo "hello hdfs" > /tmp/hello_hdfs.txt
hdfs dfs -put /tmp/hello_hdfs.txt /user/cheewap/hadoop-introduction/
hdfs dfs -ls /user/cheewap/hadoop-introduction/
hdfs dfs -cat /user/cheewap/hadoop-introduction/hello_hdfs.txt
hdfs dfs -get /user/cheewap/hadoop-introduction/hello_hdfs.txt /tmp/hello_hdfs2.txt
ll /tmp/hello_hdfs2.txt
hdfs dfs -du -h /user/cheewap/hadoop-introduction/
hdfs dfs -rm -r /user/cheewap/hadoop-introduction/*
hdfs dfs -ls /user/cheewap/hadoop-introduction/
```
