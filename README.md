# hadoop-Introduction

Introduction hadoop 's dwh

## Getting started

[Google Slide](https://docs.google.com/presentation/d/1tm0-EHWE_Hm6U_8MTZpQ1kAvQrLHeb3nncX92jF35Hg/edit?usp=sharing)


# Demo

- [HDFS](/hdfs/README.md)
- [Hive](/hive/README.md)
- [HBASE](/hbase/README.md)
- [IMPALA](/impala/README.md))
- [HUE]

# Tools
- KEYTAB
    create KERBEROS.KEYTAB สำหรับใช้งานใน job

    #### auth kerberos
    kinit {user}@TRUE.CARE
    > Password for {user}@TRUE.CARE [enter your password]

    ### check session
    klist
    ใช้ ktutil create {user}.kerberos.keytab
    ```sh
    ktutil:  addent -password -p {USERNAME}@REALM -k 1 -e rc4-hmac
    Password for {USERNAME}@REALM: [enter your password]
    ktutil:  addent -password -p {USERNAME}@REALM -k 1 -e aes256-cts
    Password for {USERNAME}@REALM: [enter your password]
    ktutil:  wkt {USERNAME}.keytab
    ktutil:  quit
    ```

- [DBeaver Impala](https://wiki.truecorp.co.th/index.php/Data_platform_hadoop_dbeaver_impala_connector)
- [DBeaver Hive](https://wiki.truecorp.co.th/index.php/Data_platform_hadoop_dbeaver_hive_connector)